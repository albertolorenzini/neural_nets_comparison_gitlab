[![Documentation Status](https://readthedocs.org/projects/neural-nets-comparison/badge/?version=latest)](https://neural-nets-comparison.readthedocs.io/en/latest/?badge=latest)

# Neural Nets Comparison
## Performance comparison between three different neural nets.

This repository tests the performance of three neural nets for object detection, in particular we use:

1. darknet-YOLO from AlexeyAB: https://github.com/AlexeyAB/darknet
2. CenterNet from xingyizhou: https://github.com/xingyizhou/CenterNet
3. SSD-Tensorflow from balancap: https://github.com/balancap/SSD-Tensorflow

For each net we trained different pre-trained weight, in order to have a better comparison. Each net has different precision and speed (as one can see following the repositories links above), in particular last characteristic (speed), is very importan for us, because the best net will work on a driverless race car.

These three repositories are not up-to-date, and their readme files often are not precise, so we created a "readme" folder in which there are all correct instructions to prepare these three nets. Furthermore, in readme folder, you can find another readme with the instructions to prepare your own datasets, that can be train on each net.

In order to be able to run all these nets on your local machine, there is a folder for each net, in which you can find all you need to run the repository dataset. Moreover, inside each file is explained (through comments) how to modify it for your own dataset. Some of these modifications are explained in the repositories linked above, but often these explanations are quite useless if not totally wrong.

Inside "utilities" folder, you can find some usefull files that can help you to prepare your own dataset (it is all explained inside readme_dataset.md) to be used by nets to train their pretrained weights.

Inside the "scripts" folder, there are files that can be used as a solution for some problems that I encountered during this work.

## Results

### YOLO

To evaluate the results of these neural nets, we take into account the Average Precision, loss and a demo, runned on each trained model. 
When yolo is training, for each epoch it updates a graph with mAP and loss. We evaluate two different model for YOLO, YOLO-V4 and tiny-YOLO:

![YOLO-V4](../images/yolov4.jpg?raw=true "YOLO-V4")
![TINY-YOLO](../images/tiny_yolo.jpg?raw=true "tiny-YOLO")

As one can see on these two graphs, we trained both YOLO models for 8000 batches, with a batch size of 32. First of all, we noted the difference in the mean Average Precision, as we expected tiny-YOLO has a low mAP of 87.3% vs 94.8% of YOLO-V4 ones, with a IoU threshold of 0.5. From the plot, we notice also that, for YOLO-V4, probably we chosen a too low batch size, in fact the loss graph is very noisy. For both graphs, the loss is almost stable (as mAP), so we do not trained the nets anymore. We will return on YOLO in the end of this readme, to take a look of its demo.

### CenterNet

CenterNet use a different type of evaluation method, we select a randomly part of images from training dataset, and we run the trained nets on this test dataset . Coco type dataset gives us an Average Precision (in coco AP and mAP are the same) divided into large, medium and small objects. It evaluates the AP with an IoU threshold that varies from 0.5 to 0.95. It does the same evaluation also without dimension divisions, but with three different thresholds: the first varies between 0.5 and 0.95, the second fixed at 0.5 and the third fixed at 0.75. Lastly it does the same alse with the Average Recall.

We trained three different models: dla_2x, resdcn_18 and resdcn_101.

![dla_2x](../images/dla2x_giusto.png?raw=true "dla_2x")
![resdcn_18](../images/results_resdcn18.png?raw=true "resdcn_18")
![resdcn_101](../images/results_resdcn101.png?raw=true "resdcn_101")

We noted immediately that the AP is very low, almost one order of magnitude low compared with YOLO. The ARs (Average Recalls), are not so bad as the APs, when we compare video demo it will become understandable. 

CenterNet gives us a log file during training in which it save one loss value for epoch:

![loss_centernet](../images/Loss_centernet.png?raw=true "loss CenterNet")

In this graph we note that for resdcn_101 model, we probably chosen a too low batch size, it was 28, the biggest for training GPU. We noted that after 125 epochs, the loss value drop stop and the val_loss stabilizes, this is the reason why for dla_2x we stopped at 140 epochs. Lastly, we noted that at epoch number 90 there is a notable loss drop, in fact at this epoch CenterNet change the learning rate from 5e-4 to 5e-5. The same change (from 5e-5 to 5e-6) is done at epoch 120, but it isn't so remarkable.

### DEMO

As we said above, we do last evaluation, running all these models on a video. My local machine, which was used for demo test, mount an Nvidia GeForce GTX 1650 (laptop).

![YOLO-V4](./yolo4_cones.gif)

Starting with YOLO V4 we can see that this net has a vary high precision. We can also note that the video is running frame-by-frame and it is not fluid, this might suggest that the computational expansive of this net is quite big.

![tiny-YOLO](./tiny_yolo4_cones.gif)

Tiny-YOLO has a less precision in object detection in comparison with YOLO-V4, as we expect. Also for classification gives us some errors. However, its mAP keep a quite high result, this should be thanks to its big bounded boxes, the ratio results between predicted bboxes and true bboxes could gives us an high score in mAP, more than a "real" one.

![CenterNet-dla_2x](./centernet_dla2x_cones.gif)
![CenterNet-resdcn_18](./centernet_resdcn18_cones.gif)
![CenterNet-resdcn_101](./centernet_resdcn101_cones.gif)

For these three models we can see a quite good precision in object detection, but a really problematic classification, this is in agree whith the APs and the ARs found above. The only acceptable ones could be dla_2x model, but in comparison with YOLO-V4, this lastone remain the best ones in terms of precision. On the other side, CenterNet seems to be a light net (from computational expansive view).

After these observations, the most precise model seems to be YOLO-V4, which could be problematic on the computational expansive side. However, our car will mount two Nvidia Jetson AGX Xavier, so the lag encountered during our YOLO-V4 demo shouldn't be a problem for us. 

In conclusion, for the E-Team Squadra Corse Driverless car (Kerubless), the choice should be YOLO-V4.
