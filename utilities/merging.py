"""
merge different annotations and images dataset in a unique one
##############################################################

author = Alberto Lorenzini

******
usage:
******

it request COCO-assistant to work, you can install it using pip install coco-assistant or from source: https://github.com/ashnair1/COCO-Assistant

insert images and annotations directories, where your files are located, in the last sections of this file (there is a comment where to do it).
You have to have all of your images in one folder, and all of your annotations in another folder.

Then run the following command::

    python merging.py


"""


import os
from coco_assistant import COCO_Assistant


def merging(images_directory, annotations_directory):
    """
    the two parameters of this function are image and annotations directory, and you have to replace them with your chosen ones.
    """

    img_dir = os.path.join(os.getcwd(), images_directory)
    ann_dir = os.path.join(os.getcwd(), annotations_directory)

    # Create COCO_Assistant object
    cas = COCO_Assistant(img_dir, ann_dir)
    cas.merge(merge_images=True)

if __name__ == '__main__':

    ######################################################################
    # insert here where are all your images and all your annotations files
    ######################################################################

    images_directory = './images'
    annotations_directory = './annotations'