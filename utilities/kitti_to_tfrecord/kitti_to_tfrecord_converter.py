"""
This file convert kitti object format to tfrecord for SSD-TensorFlow
####################################################################

author = Alberto Lorenzini

it request tensorflow, but for SSD-tensorflow you need tensorflow 1.x. You can install it with the following command::

    conda install -c conda-forge tensorflow

-----
Usage
-----

You have to insert the kitti dataset path, chose what conversion type you want, between train val and trainval and the output directory.

Example::


    python tf_convert_data.py \
        --datset_root=path/to/kittyobjectdataset \
        --split=trainval \
        --output_dir=./kitti_tfrecord
"""


import tensorflow as tf
import kitti_object_to_tfrecords


FLAGS = tf.app.flags.FLAGS


tf.app.flags.DEFINE_string(
    'dataset_name', 'kitti',
    'The name of the dataset to convert.')
tf.app.flags.DEFINE_string(
    'dataset_root', None,
    'Directory where the original dataset is stored.')
tf.app.flags.DEFINE_string(
    'split', 'trainval',
    'Split of dataset, trainval/train/val/test.')
tf.app.flags.DEFINE_string(
    'output_dir', './',
    'Output directory where to store TFRecords files.')


def main(_):
    """
    this function have no parameters, it print dataset root directory and output directory, then it make a control the dataset type is kitti,
    then run conversion via :doc:`kitti_object_to_tfrecords`.
    """
    print('Dataset root dir:', FLAGS.dataset_root)
    print('Output directory:', FLAGS.output_dir)

    if FLAGS.dataset_name == 'kitti':
        kitti_object_to_tfrecords.run(FLAGS.dataset_root,
                                   FLAGS.split,
                                   FLAGS.output_dir,
                                   shuffling=True)
    else:
        raise ValueError('Dataset [%s] was not recognized.' % FLAGS.dataset_name)


if __name__ == '__main__':
    tf.app.run()

