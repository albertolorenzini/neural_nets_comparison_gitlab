"""
is a program that takes files from a directory written in a file txt, and copies that files in another new directory
####################################################################################################################

author = Alberto Lorenzini

-----
usage
-----

Open it and change the directories with your chosen ones, in the last part there are comments that indicates where you have to do some changes.

"""




import shutil
import os


def movingmoving(reading_trainfile, reading_testfile, reading_train_annotations, reading_test_annotations, destination_path_train, destination_path_test):

    """
    All parameters of this function are directories and you have to replace them with your chosen ones
    """
    #############################################################################################################
    # it reads the file in which it is write the directory's images for trainig and put the directories in a list
    #############################################################################################################

    with open(reading_trainfile) as trainfile:
        original_path_train = trainfile.readlines()

    original_path_train = [x.strip() for x in original_path_train]

    #the same as above but for testing
    with open(reading_testfile) as testfile:
        original_path_test = testfile.readlines()

    x = 0
    original_path_test = [x.strip() for x in original_path_test]




    ###################################################################################################################
    # it reads the file in which it is write the directory's annotations for training and put the directories in a list
    ###################################################################################################################

    with open(reading_train_annotations) as train_annotationfile:
        original_path_train_annotation = train_annotationfile.readlines()

    x = 0
    original_path_train_annotation = [x.strip() for x in original_path_train_annotation]

    #the same as above but for testing
    with open(reading_test_annotations) as test_annotationfile:
        original_path_test_annotation = test_annotationfile.readlines()

    x = 0
    original_path_test_annotation = [x.strip() for x in original_path_test_annotation]


    ######################################################################
    # create new directory for training and testing images and annotations
    ######################################################################

    os.mkdir(destination_path_train)

    os.mkdir(destination_path_test)


    ############################################
    # It copies training files in such directory
    ############################################

    x=0
    y=0
    for x in original_path_train:
        shutil.copy(original_path_train[y], destination_path_train)
        shutil.copy(original_path_train_annotation[y], destination_path_train)
        y+=1

    ###########################################
    # it copies testing files in such directory
    ###########################################

    x=0
    y=0
    
    for x in original_path_test:
        shutil.copy(original_path_test[y], destination_path_test)
        shutil.copy(original_path_test_annotation[y], destination_path_test)
        y+=1


if __name__ == '__main__':

    ###############################################################
    #                                                             #
    # Here you have to change all directories in your chosen ones #
    #                                                             #
    ###############################################################

    reading_trainfile = '../darknet_utilities/train.txt'
    reading_testfile = '../darknet_utilities/test.txt'
    reading_train_annotations = '../darknet_utilities/train_annotations.txt'
    reading_test_annotations = '../darknet_utilities/test_annotations.txt'
    destination_path_train = "../CenterNet_utilities/train"
    destination_path_test = "../CenterNet_utilities/test"
