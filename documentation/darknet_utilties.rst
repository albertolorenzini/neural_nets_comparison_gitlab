darknet utilties
================

Inside darknet_utilties, the only files that you may need to change are the .txt files. Inside them there are path directories in which are located darknet format dataset.

All lines in these files are quite the same, a part for the file's names. So you can simply use "find a replace" to replace the default directories with your correct ones. The train_annotations.txt and test_annotations.txt files are exactly the same as train.txt and test.txt files, the only difference is the files extensions: annotations files directories are .txt and then they search for annotations darknet format, train and test files directories are .jpg and then search for images.

Here an example to explain what i mean:

train.txt code:

	/home/darknet/data/obj/darknet_cones/FaSTTUBe/92.jpg
	/home/darknet/data/obj/darknet_cones/FaSTTUBe/15.jpg
	/home/darknet/data/obj/darknet_cones/FaSTTUBe/151.jpg
	/home/darknet/data/obj/darknet_cones/FaSTTUBe/499.jpg
	

train_annotations.txt code:

	/home/darknet/data/obj/darknet_cones/FaSTTUBe/92.txt
	/home/darknet/data/obj/darknet_cones/FaSTTUBe/15.txt
	/home/darknet/data/obj/darknet_cones/FaSTTUBe/151.txt
	/home/darknet/data/obj/darknet_cones/FaSTTUBe/499.txt
