scripts
=======

Inside scripts folder, there are three scripts, one each possible utilization between train, val and trainval. They are usefull to create a tfrecord dataset format, starting from kitti annotations. How to use them is explained inside :any:`readme_dataset`.

train_imagesets
---------------

This is the content of train_imagesets.sh::

	cd /path/to/kitti_cones
	mkdir ImageSets
	cd ./ImageSets
	ls ../training/image_2/ | grep ".jpg" | sed s/.jpg// > train.txt
	
val_imagesets
--------------

There are some difference between these three scripts, val_imagesets.sh take images name from testing folder and put them inside a file called val.txt::

	cd /path/to/kitti_cones
	mkdir ImageSets
	cd ./ImageSets
	ls ../testing/image_2/ | grep ".jpg" | sed s/.jpg// > val.txt
	
trainval_imagesets
------------------

Lastly, trainval_imagesets.sh take all images name inside training and testing folder and put them inside a file called trainval.txt::

	cd /path/to/kitti_cones
	mkdir ImageSets
	cd ./ImageSets
	ls ../training/image_2/ | grep ".jpg" | sed s/.jpg// > trainval.txt
	ls ../testing/image_2/ | grep ".jpg" | sed s/.jpg// > trainval.txt
